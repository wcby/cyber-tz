<?php

namespace App\Models;
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model as BaseModel;

class Model extends BaseModel {
    use HasFactory;

    protected $fillable = ['name', 'url'];

    public function generations() {
        return $this->hasMany(Generation::class);
    }
}
