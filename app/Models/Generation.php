<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model as BaseModel;

class Generation extends BaseModel {
    use HasFactory;

    protected $fillable = ['model_id', 'market', 'name', 'period', 'generation', 'image_path', 'specs_path'];

    public function model() {
        return $this->belongsTo(Model::class);
    }
}
