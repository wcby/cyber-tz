<?php

namespace App\Services;

use Symfony\Component\Panther\PantherTestCase;
use Symfony\Component\DomCrawler\Crawler;
use App\Models\Model;

class ModelParsingService extends PantherTestCase
{
    public function parse()
    {
        $client = static::createPantherClient();
        $crawler = $client->request('GET', 'https://www.drom.ru/catalog/audi/');

        $crawler->filter('a.g6gv8w4.g6gv8w8._501ok20')->each(function (Crawler $node) {
            $name = $node->text();
            $url = $node->attr('href');

            Model::updateOrCreate(
                ['name' => $name],
                ['url' => $url]
            );
        });
    }
}
