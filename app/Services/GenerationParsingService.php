<?php
<?php

namespace App\Services;

use Symfony\Component\Panther\PantherTestCase;
use Symfony\Component\DomCrawler\Crawler;
use App\Models\Model;
use App\Models\Generation;

class GenerationParsingService extends PantherTestCase
{
    public function parse()
    {
        $models = Model::all();

        $client = static::createPantherClient();

        foreach ($models as $model) {
            $crawler = $client->request('GET', $model->url);

            // Парсинг поколений по рынкам
            $markets = ['#russia', '#japan', '#europe', '#usa'];
            foreach ($markets as $market) {
                $this->parseMarket($crawler, $market, $model->id);
            }
        }
    }

    protected function parseMarket(Crawler $crawler, $market, $modelId)
    {
        $crawler->filter($market . ' .css-0.e1ei9t6a0')->each(function (Crawler $node) use ($market, $modelId) {
            $generationInfo = $node->text();
            $imagePath = $node->filter('img')->attr('src');
            $specsPath = $node->filter('a')->attr('href');

            Generation::updateOrCreate(
                ['model_id' => $modelId, 'market' => $market],
                ['name' => $generationInfo, 'image_path' => $imagePath, 'specs_path' => $specsPath]
            );
        });
    }
}
