<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Console\Commands\ParseModelsCommand;
use App\Console\Commands\ParseAudiGenerationsCommand;

class AppServiceProvider extends ServiceProvider
{
    public function register()
    {
        // Регистрация команды parse:models
        $this->app->bind('command.parse:models', function () {
            return new ParseModelsCommand();
        });

        $this->commands([
            'command.parse:models',
        ]);

        // Регистрация команды parse:audi-generations
        $this->app->bind('command.parse:generations', function () {
            return new ParseGenerationsCommand();
        });

        $this->commands([
            'command.parse:generations',
        ]);
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
    }
}
