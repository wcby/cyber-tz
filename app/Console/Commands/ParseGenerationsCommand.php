<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Model;
use App\Services\GenerationParsingService;

class ParseGenerationsCommand extends Command {
    protected $signature = 'parse:generations';
    protected $description = 'Парсинг поколений для моделей';
    protected $service;

    public function __construct(GenerationParsingService $service) {
        parent::__construct();
        $this->service = $service;
    }

    public function handle() {
        $models = Model::all();

        foreach ($models as $model) {
            $this->service->parseGenerations($model);
        }

        $this->info('Поколения успешно спарсены и сохранены.');
    }
}
