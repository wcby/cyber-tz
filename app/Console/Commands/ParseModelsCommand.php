<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\ModelParsingService;

class ParseModelsCommand extends Command {
    protected $signature = 'parse:models';
    protected $description = 'Парсинг модельного ряда Audi';
    protected $modelParsingService;

    public function __construct(ModelParsingService $modelParsingService) {
        parent::__construct();
        $this->modelParsingService = $modelParsingService;
    }

    public function handle() {
        $this->modelParsingService->parse();
        $this->info('Модели успешно спарсены и сохранены.');
    }
}

